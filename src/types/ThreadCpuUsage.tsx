export default class ThreadCpuUsage {
  public id!: number;
  public cpuUsage!: number;
  public runningFor!: string;
}
